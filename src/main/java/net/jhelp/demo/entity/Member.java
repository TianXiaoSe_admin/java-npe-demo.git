package net.jhelp.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <ul>
 *     会员信息表
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 1:33 PM
 */
@Data

public class Member {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 会员呢称
     */
    private String nickName;

    /**
     *  真实姓名
     */
    private String realName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 其他属性
     */

    public Member(){

    }

    public Member(Integer id, Integer memberId, String nickName, String realName, Integer gender) {
        this.id = id;
        this.memberId = memberId;
        this.nickName = nickName;
        this.realName = realName;
        this.gender = gender;
    }
}
