package net.jhelp.demo.entity;

import lombok.Data;

/**
 * <ul>
 *     部门表
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 5:53 PM
 */
@Data
public class Dept {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  部门id
     */
    private Integer deptId;
    /**
     * 父ID
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 其他属性
     */
}
