package net.jhelp.demo.service;

import net.jhelp.demo.OptionalUtils;
import net.jhelp.demo.ProgrammerA;
import net.jhelp.demo.dto.DeptDTO;
import net.jhelp.demo.dto.MemberDTO;
import net.jhelp.demo.dto.MemberDTO2;
import net.jhelp.demo.entity.Dept;
import net.jhelp.demo.entity.Member;

import java.util.Optional;

/**
 * <ul>
 *     假设有一个member（会员）的服务类，提供会员的一系列操作
 *     包括查询数据库，类型转换
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 10:40 AM
 */
public class MemberService {

    /**
     * 从数据库中获取memberId的会员信息
     * @param memberId
     *  会员id
     * @return
     */
    public Member getMember(Integer memberId){
        /**
         * 模似从数据库中获取一条记录
         */
        Member member = new Member();

        // .... 属性

        return member;
    }

    /**
     * 传统的定法，就是先判断对象是否为null，不为null，
     * 则进行转换操作，否则，返回null
     * @param member
     *      输入的会员对象
     * @return
     */
    public MemberDTO transfer(Member member){
        if(null != member){
            MemberDTO memberDTO = new MemberDTO()
                    .setMemberId(member.getMemberId())
                    .setGender(member.getGender())
                    .setGenderName(ProgrammerA.getGender2(member.getGender()))
                    .setNickName(member.getNickName())
                    .setRealName(member.getRealName());
            return memberDTO;
        }
        return null;
    }

    /**
     * 通过Optional的方式来处理转换，减少编写 xx != null 这样的表达式
     * @param member
     *      输入的会员信息
     * @param defaultDto
     *      默认的值（如果为null时）
     * @return
     */
    public MemberDTO transferByOptional(Member member, MemberDTO defaultDto){
        return OptionalUtils.transfer(member, MemberService::convert, defaultDto);
    }

    /**
     * 将member的属性 赋值给memberDTO
     * @param member
     *      输入的会员信息
     * @return
     */
    public static MemberDTO convert(Member member){
        MemberDTO memberDTO = new MemberDTO()
                .setMemberId(member.getMemberId())
                .setGender(member.getGender())
                .setGenderName(ProgrammerA.getGender2(member.getGender()))
                .setNickName(member.getNickName())
                .setRealName(member.getRealName());
        return memberDTO;
    }

    /**
     * 通过Optional的方式来处理转换，减少编写 xx != null 这样的表达式
     * @param member
     *      输入的会员信息
     * @param defaultDto
     *      默认的值（如果为null时）
     * @return
     */
    public MemberDTO2 transferByOptional2(Member member, MemberDTO2 defaultDto){
        return OptionalUtils.transfer(member, MemberService::convert2, defaultDto);
    }


    /**
     * 将member的属性 赋值给memberDTO
     * @param member
     *      输入的会员信息
     * @return
     */
    public static MemberDTO2 convert2(Member member){
        MemberDTO2 memberDTO = new MemberDTO2()
                .setMemberId(member.getMemberId())
                .setGender(member.getGender())
                .setGenderName(ProgrammerA.getGender2(member.getGender()))
                .setNickName(member.getNickName())
                .setRealName(member.getRealName());
        return memberDTO;
    }
    /**
     * 将部门对象转换成DTO
     * @param dept
     * @return
     */
    public static DeptDTO cover(Dept dept){
        DeptDTO deptDTO = new DeptDTO();
        deptDTO.setDeptId(dept.getDeptId());
        deptDTO.setParentId(dept.getParentId());
        deptDTO.setDeptName(dept.getDeptName());
        return deptDTO;
    }
}
