package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import net.jhelp.demo.dto.ShopSaleDataDTO;
import net.jhelp.demo.entity.OrderSaleData;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * <ul>
 *     Null数据转换测试类
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/19 10:16 AM
 */
@Slf4j
public class NullValueTest2 {

    @Test
    public void nullValueTest(){
        OrderSaleData orderSaleData = new OrderSaleData()
                .setShopId(1000)
                .setLogDate(new java.util.Date())
                .setOrderAmount(new BigDecimal(988.1234))
                .setOrderCount(100)
                .setOrderGmvAmount(new BigDecimal(2012.1265))
                .setSaleSkuCount(300)
                .setBuyerCount(34)
                .setAvgBuyerPrice(new BigDecimal(33.12));

        ShopSaleDataDTO dto2 = toDTO2(orderSaleData);
        log.debug("Optional模板方法结果：{}", dto2);

        ShopSaleDataDTO dto3 = toDTO3(orderSaleData);
        log.debug("Optional+多元化方法结果：{}", dto3);

        OrderSaleData nullOrderData = new OrderSaleData()
                .setShopId(1000)
                .setOrderCount(11)
                ;

        ShopSaleDataDTO dto4 = toDTO3(nullOrderData);
        log.debug("数值为null时结果（带默认值）：{}", dto4);

    }



    /**
     * 通过Optional的特性，定义模板方法来抽象对 obj == null的重复代码
     * @param orderSaleData
     * @return
     */
    private ShopSaleDataDTO toDTO2(OrderSaleData orderSaleData){
        ShopSaleDataDTO shopSaleDataDTO = new ShopSaleDataDTO();
        //使用Optional模板方法
        shopSaleDataDTO.setShopId(orderSaleData.getShopId());
        shopSaleDataDTO.setLogDate(ValueUtilsV2.formatDate(orderSaleData.getLogDate(), ""));
        shopSaleDataDTO.setOrderCount(ValueUtilsV2.wrapNull(orderSaleData.getOrderCount(), 0));
        shopSaleDataDTO.setBuyerCount(ValueUtilsV2.wrapNull(orderSaleData.getBuyerCount(), 0));
        shopSaleDataDTO.setSaleSkuCount(ValueUtilsV2.wrapNull(orderSaleData.getSaleSkuCount(), 0));
        shopSaleDataDTO.setOrderAmount(ValueUtilsV2.wrapNull(orderSaleData.getOrderAmount(), 2, BigDecimal.ZERO));
        shopSaleDataDTO.setOrderGmvAmount(ValueUtilsV2.wrapNull(orderSaleData.getOrderGmvAmount(), 2, BigDecimal.ZERO));
        shopSaleDataDTO.setAvgBuyerPrice(ValueUtilsV2.wrapNull(orderSaleData.getAvgBuyerPrice(), 2, BigDecimal.ZERO));
        return shopSaleDataDTO;
    }

    /**
     *  通过对null转换的方法的改造(多元化），实现更简洁的代码
     *
     * @param orderSaleData
     * @return
     */
    private ShopSaleDataDTO toDTO3(OrderSaleData orderSaleData){
        ShopSaleDataDTO shopSaleDataDTO = new ShopSaleDataDTO();
        //更简单的方式
        shopSaleDataDTO.setShopId(orderSaleData.getShopId());
        shopSaleDataDTO.setLogDate(ValueUtilsV2.formatDate(orderSaleData.getLogDate(), "yyyy-MM-dd"));
        shopSaleDataDTO.setOrderCount(ValueUtilsV2.nullInt(orderSaleData.getOrderCount()));
        shopSaleDataDTO.setBuyerCount(ValueUtilsV2.nullInt(orderSaleData.getBuyerCount()));
        shopSaleDataDTO.setSaleSkuCount(ValueUtilsV2.nullInt(orderSaleData.getSaleSkuCount()));
        shopSaleDataDTO.setOrderAmount(ValueUtilsV2.wrapNull(orderSaleData.getOrderAmount()));
        shopSaleDataDTO.setOrderGmvAmount(ValueUtilsV2.wrapNull(orderSaleData.getOrderGmvAmount()));
        shopSaleDataDTO.setAvgBuyerPrice(ValueUtilsV2.wrapNull(orderSaleData.getAvgBuyerPrice()));
        return shopSaleDataDTO;
    }
}
