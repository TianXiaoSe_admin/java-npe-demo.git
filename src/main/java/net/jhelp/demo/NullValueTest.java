package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import net.jhelp.demo.dto.ShopSaleDataDTO;
import net.jhelp.demo.entity.OrderSaleData;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * <ul>
 *     Null数据转换测试类
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/19 10:16 AM
 */
@Slf4j
public class NullValueTest {

    @Test
    public void nullValueTest(){
        //java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result.
        OrderSaleData orderSaleData = new OrderSaleData()
                .setShopId(1000)
                .setLogDate(new java.util.Date())
                .setOrderAmount(new BigDecimal(988.1234))
                .setOrderCount(100)
                .setOrderGmvAmount(new BigDecimal(2012.1265))
                .setSaleSkuCount(300)
                .setBuyerCount(34)
                .setAvgBuyerPrice(new BigDecimal(29.12));

        ShopSaleDataDTO dto1 = toDTO1(orderSaleData);
        log.debug("传统方法结果：{}", dto1);

        ShopSaleDataDTO dto2 = toDTO2(orderSaleData);
        log.debug("Optional模板方法结果：{}", dto2);

        ShopSaleDataDTO dto3 = toDTO3(orderSaleData);
        log.debug("Optional+多元化方法结果：{}", dto3);

    }

    /**
     *  比较常见的赋值过程，
     *  通过判断值是否为null，或者三元式来处理
     * @param orderSaleData
     * @return
     */
    private ShopSaleDataDTO toDTO1(OrderSaleData orderSaleData){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        ShopSaleDataDTO shopSaleDataDTO = new ShopSaleDataDTO();
        shopSaleDataDTO.setShopId(orderSaleData.getShopId());
        shopSaleDataDTO.setLogDate(orderSaleData.getLogDate() == null ? "" : sdf.format(orderSaleData.getLogDate()));

        shopSaleDataDTO.setBuyerCount(orderSaleData.getBuyerCount() == null ? 0 : orderSaleData.getBuyerCount());
        shopSaleDataDTO.setOrderCount(orderSaleData.getOrderCount() == null ? 0 : orderSaleData.getOrderCount());
        shopSaleDataDTO.setOrderAmount(orderSaleData.getOrderAmount() == null ? BigDecimal.ZERO : orderSaleData.getOrderAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
        shopSaleDataDTO.setSaleSkuCount(orderSaleData.getSaleSkuCount() == null ? 0 : orderSaleData.getSaleSkuCount());
        shopSaleDataDTO.setOrderGmvAmount(orderSaleData.getOrderGmvAmount() == null ? BigDecimal.ZERO : orderSaleData.getOrderGmvAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
        shopSaleDataDTO.setAvgBuyerPrice(orderSaleData.getAvgBuyerPrice() == null ? BigDecimal.ZERO : orderSaleData.getAvgBuyerPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
        return shopSaleDataDTO;
    }

    /**
     * 通过Optional的特性，定义模板方法来抽象对 obj == null的重复代码
     * @param orderSaleData
     * @return
     */
    private ShopSaleDataDTO toDTO2(OrderSaleData orderSaleData){
        ShopSaleDataDTO shopSaleDataDTO = new ShopSaleDataDTO();
        //使用Optional模板方法
        shopSaleDataDTO.setShopId(orderSaleData.getShopId());
        shopSaleDataDTO.setLogDate(ValueUtils.formatDate(orderSaleData.getLogDate(), ""));
        shopSaleDataDTO.setOrderCount(ValueUtils.wrapNull(orderSaleData.getOrderCount(), 0));
        shopSaleDataDTO.setBuyerCount(ValueUtils.wrapNull(orderSaleData.getBuyerCount(), 0));
        shopSaleDataDTO.setSaleSkuCount(ValueUtils.wrapNull(orderSaleData.getSaleSkuCount(), 0));
        shopSaleDataDTO.setOrderAmount(ValueUtils.wrapNull(orderSaleData.getOrderAmount(), 2, BigDecimal.ZERO));
        shopSaleDataDTO.setOrderGmvAmount(ValueUtils.wrapNull(orderSaleData.getOrderGmvAmount(), 2, BigDecimal.ZERO));
        shopSaleDataDTO.setAvgBuyerPrice(ValueUtils.wrapNull(orderSaleData.getAvgBuyerPrice(), 2, BigDecimal.ZERO));
        return shopSaleDataDTO;
    }

    /**
     *  通过对null转换的方法的改造(多元化），实现更简洁的代码
     *
     * @param orderSaleData
     * @return
     */
    private ShopSaleDataDTO toDTO3(OrderSaleData orderSaleData){
        ShopSaleDataDTO shopSaleDataDTO = new ShopSaleDataDTO();
        //更简单的方式
        shopSaleDataDTO.setShopId(orderSaleData.getShopId());
        shopSaleDataDTO.setLogDate(ValueUtils.formatDate(orderSaleData.getLogDate(), "yyyy-MM-dd"));
        shopSaleDataDTO.setOrderCount(ValueUtils.nullInt(orderSaleData.getOrderCount()));
        shopSaleDataDTO.setBuyerCount(ValueUtils.nullInt(orderSaleData.getBuyerCount()));
        shopSaleDataDTO.setSaleSkuCount(ValueUtils.nullInt(orderSaleData.getSaleSkuCount()));
        shopSaleDataDTO.setOrderAmount(ValueUtils.wrapNull(orderSaleData.getOrderAmount()));
        shopSaleDataDTO.setOrderGmvAmount(ValueUtils.wrapNull(orderSaleData.getOrderGmvAmount()));
        shopSaleDataDTO.setAvgBuyerPrice(ValueUtils.wrapNull(orderSaleData.getAvgBuyerPrice()));
        return shopSaleDataDTO;
    }
}
