package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * <ul>
 *     BigDecimal 测试类
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/28 10:16 AM
 */
@Slf4j
public class BigDecimalTest {

    @Test
    @Deprecated
    public void test(){
        //省略业务的逻辑
        //得到订单金额
        BigDecimal orderAmount = new BigDecimal(2312.23);
        //得到成本
        BigDecimal cost = new BigDecimal(984.23);
        //得到客户数
        BigDecimal customerCount = new BigDecimal(35);
        //得到客单价
        BigDecimal pricePerCustomer = orderAmount.divide(customerCount, 2, BigDecimal.ROUND_HALF_UP);

        log.info("客单价：{}", pricePerCustomer);

        //得到毛利率
        BigDecimal profit = (orderAmount.subtract(cost)).divide(cost, 2, BigDecimal.ROUND_HALF_UP);
        log.info("毛利率：{}", profit);

        //省去其他逻辑
    }

    @Test
    public void test2(){
        //省略业务的逻辑
        //得到订单金额
        Double orderAmount = new Double(2312.23);
        //得到成本
        Double cost = new Double(984.23);
        //得到客户数
        Double customerCount = new Double(35);
        //得到客单价
        Double pricePerCustomer = orderAmount / customerCount;

        //得到毛利率
        Double profit = (orderAmount - cost) / cost;

        log.info("客单价：{}", pricePerCustomer);
        log.info("毛利率：{}", profit);

        //省去其他逻辑
    }
}
