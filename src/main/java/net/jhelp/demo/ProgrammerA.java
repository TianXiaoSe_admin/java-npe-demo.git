package net.jhelp.demo;

import com.sun.istack.internal.NotNull;

import java.util.Objects;

/**
 * <ul>
 *     小伙伴A的方法
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 2:31 PM
 */
public class ProgrammerA {

    /**
     * 假设项目中有一个方法，通过输入的类型，来输出男女
     * 程序设定1 - 男， 2-女， 其他-未设置
     */
    public static String getGender(int gender){
        if(gender == 1){
            return "男";
        }else if(gender == 2){
            return "女";
        }else {
            return "未设置";
        }
    }

    /**
     * 通过输入的类型，来输出男女
     * 程序设定1 - 男， 2-女， 其他-未设置
     */
    public static String getGender2(@NotNull Integer gender){
        if(Objects.equals(gender, 1)){
            return "男";
        }else if(Objects.equals(gender, 2)){
            return "女";
        }else {
            return "未设置";
        }
    }
}
