package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import net.jhelp.demo.dto.MemberDTO;
import net.jhelp.demo.entity.Member;
import net.jhelp.demo.service.MemberService;

/**
 * <ul>
 *     包装类型与非包装类的 测试例子
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 10:37 AM
 */
@Slf4j
public class ProgrammerB {

    /**
     * 假设这里有一个MemberService
     */
    private MemberService memberService = new MemberService();

    /**
     *  模拟小伙伴B调用小伙伴A的方法
     */
    public MemberDTO getMemberInfo(Integer memberId){
        //从数据库中获取某个会员的信息，
        Member member = memberService.getMember(memberId);
        String genderName = ProgrammerA.getGender(member.getGender());

        MemberDTO memberDTO = new MemberDTO()
                .setMemberId(member.getMemberId())
                .setGender(member.getGender())
                .setGenderName(genderName)
                .setNickName(member.getNickName())
                .setRealName(member.getRealName());

        return memberDTO;
    }


}
