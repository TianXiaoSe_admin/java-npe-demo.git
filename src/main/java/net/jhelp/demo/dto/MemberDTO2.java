package net.jhelp.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <ul>
 *     会员对外的DTO
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 2:39 PM
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class MemberDTO2 extends EmptyDTO{

    /**
     * 带参数的构造函数（是否空）
     * @param empty
     */
    public MemberDTO2(Boolean empty){
        setEmpty(empty);
    }
    /**
     * 定义一个空的对象（通过 isEmpty来判断是否为空）
     */
    public static final MemberDTO2 EMPTY_MEMBER = new MemberDTO2(true);


    private Integer memberId;
    /**
     * 会员呢称
     */
    private String nickName;

    /**
     *  真实姓名
     */
    private String realName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 性别名称
     */
    private String genderName;


    /**
     * 其他属性
     */

}
