package net.jhelp.demo.dto;

import lombok.Data;

/**
 * <ul>
 *     空对象
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 3:59 PM
 */
@Data
public abstract class EmptyDTO {

    private Boolean empty = false;

    public EmptyDTO(){}

    public EmptyDTO(Boolean empty){
        this.empty = empty;
    }

    public Boolean isEmpty(){
        return this.empty;
    }
}
