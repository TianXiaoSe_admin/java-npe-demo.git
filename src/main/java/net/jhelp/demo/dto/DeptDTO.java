package net.jhelp.demo.dto;

import lombok.Data;

/**
 * <ul>
 *     部门对外的DTO
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 5:54 PM
 */
@Data
public class DeptDTO extends EmptyDTO {
    public DeptDTO(){}

    public DeptDTO(Boolean empty){
        setEmpty(empty);
    }

    /**
     *  部门id
     */
    private Integer deptId;
    /**
     * 父ID
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 其他属性
     */
}
