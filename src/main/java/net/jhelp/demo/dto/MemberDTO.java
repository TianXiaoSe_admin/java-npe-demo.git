package net.jhelp.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <ul>
 *     会员对外的DTO
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 2:39 PM
 */
@Data
@Accessors(chain = true)
public class MemberDTO extends EmptyDTO{


    /**
     * 定义一个空的对象
     */
    public static final MemberDTO EMPTY_MEMBER = new MemberDTO();


    private Integer memberId;
    /**
     * 会员呢称
     */
    private String nickName;

    /**
     *  真实姓名
     */
    private String realName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 性别名称
     */
    private String genderName;


    /**
     * 其他属性
     */

}
