package net.jhelp.demo.dto;

import lombok.Data;

import java.math.BigDecimal;


/**
 * <ul></ul>
 *
 * @author : kame
 * @date: 2022/4/19 10:27 AM
 */
@Data
public class ShopSaleDataDTO {
    /**
     * 商家id
     */
    private Integer shopId;

    /**
     *  统计日期
     */
    private String logDate;

    /**
     * 订单数
     */
    private Integer orderCount;

    /**
     * 订单销售金额
     */
    private BigDecimal orderAmount;

    /**
     * GMV 金额
     */
    private BigDecimal orderGmvAmount;

    /**
     * 买家数量
     */
    private Integer buyerCount;

    /**
     * 销售sku件数
     */
    private Integer saleSkuCount;

    /**
     * 平均客单价
     */
    private BigDecimal avgBuyerPrice;
}
