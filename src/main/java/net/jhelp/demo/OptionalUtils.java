package net.jhelp.demo;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <ul>
 *     属性互转工具类
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 3:41 PM
 */
public class OptionalUtils {

    /**
     * 属性互转模板方法
     * @param source
     *  原对象（需要比较的对象）
     * @param function
     *  对象E->R的赋值过程
     * @param defaultObject
     *  默认值
     * @param <E>
     *      原始对象
     * @param <R>
     *      结果对象
     * @return
     */
    public static <E, R> R transfer(E source, Function<E, R> function, R defaultObject){
        //旧的逻辑处理逻辑
//        Optional<E> optional = Optional.ofNullable(source);
//        if(optional.isPresent()){
//            return function.apply(optional.get());
//        }
//        return defaultObject;

        //利用orElse后的处理逻辑
        return Optional.ofNullable(source).map(e->function.apply(e)).orElse(defaultObject);
    }

    /**
     * 获取对象的属性（带判断null)
     * @param source
     *  原对象（需要比较的对象）
     * @param supplier
     *  工厂方法
     * @param defaultObject
     *  默认值
     * @param <E>
     *     输入的对象
     * @param <R>
     *     输出的值
     * @return
     */
    public static <E, R> R getAttr(E source, Supplier<R> supplier, R defaultObject) {
        //原来的处理逻辑
//      Optional<E> optional = Optional.ofNullable(source);
//      if(optional.isPresent()){
//          return supplier.get();
//      }else{
//          return defaultObject;
//      }
        //利用orElse后的逻辑
        return Optional.ofNullable(source).map(e->supplier.get()).orElse(defaultObject);
    }


}
