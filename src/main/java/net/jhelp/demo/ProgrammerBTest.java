package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import net.jhelp.demo.entity.Member;
import org.junit.Test;

/**
 * <ul>
 *     包装类型与非包装类的 测试例子
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/15 10:37 AM
 */
@Slf4j
public class ProgrammerBTest {

    @Test
    public void getGenderTest(){
        //假设不是从数据库中获取数据，而是在程序处理过中，
        Member member = new Member();
        member.setId(1);
        member.setMemberId(1000);
        member.setNickName("测试");
        member.setGender(1);
        log.debug("{}", ProgrammerA.getGender(member.getGender()));
    }

    /**
     * 测试方法没有处理null的情况
     */
    @Test
    public void getGenderWithNull(){
        Integer gender = null;
        log.debug("{}", ProgrammerA.getGender(gender));
    }
    //跑一下上面的测试用例，程序出异常

    /**
     * 测试getGender方法调整后的情况
     */
    @Test
    public void getGenderWithNull2(){
        Integer gender = null;
        log.debug("{}", ProgrammerA.getGender2(gender));
    }
}
