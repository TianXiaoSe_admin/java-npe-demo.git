package net.jhelp.demo;

import lombok.extern.slf4j.Slf4j;
import net.jhelp.demo.dto.DeptDTO;
import net.jhelp.demo.dto.MemberDTO;
import net.jhelp.demo.dto.MemberDTO2;
import net.jhelp.demo.entity.Dept;
import net.jhelp.demo.entity.Member;
import net.jhelp.demo.service.MemberService;
import org.junit.Test;

import java.util.Optional;

/**
 * <ul></ul>
 *
 * @author : kame
 * @date: 2022/4/15 3:55 PM
 */
@Slf4j
public class OptionalTest {

    private MemberService memberService = new MemberService();

    /**
     * 测试正常情况下的调用（非null）
     */
    @Test
    public void optionalTest(){
        // 声明一个会员
        Member member = new Member();
        member.setId(1);
        member.setMemberId(1000);
        member.setNickName("测试");
        member.setGender(1);

        MemberDTO memberDTO = memberService.transferByOptional(member, null);
        log.debug("{}", memberDTO);

        //声明一个部门
        Dept dept = new Dept();
        dept.setId(1);
        dept.setDeptId(100);
        dept.setParentId(0);
        dept.setDeptName("部门");

        DeptDTO deptDTO = OptionalUtils.transfer(dept, MemberService::cover, null);
        log.debug("deptDTO : {}", deptDTO);
    }

    /**
     *  测试传入对象是null的情况
      */
    @Test
    public void optionalWithNullTest(){
        MemberDTO memberDTO = memberService.transferByOptional(null, null);
        log.debug("optionalWithNullTest：{}", memberDTO);
    }

    /**
     *  测试传入对象是null的情况
     */
    @Test
    public void optionalWithNullTest2(){
        MemberDTO2 memberDTO = memberService.transferByOptional2(null, MemberDTO2.EMPTY_MEMBER);
        log.debug("optionalWithNullTest2：{}", memberDTO);
        log.debug("是否是空对象：{}", memberDTO.isEmpty());
    }

    /**
     * 测试从对象中获取某个值
     */
    @Test
    public void propertiesGetTest(){
        Member member = new Member();
        member.setId(1);
        member.setMemberId(1000);
        member.setNickName("测试");
        member.setGender(1);
        member.setRealName("java");

        String attr = OptionalUtils.getAttr(member, ()-> member.getNickName(), null);
        log.debug("nickName: {} ", attr);

        String realName = OptionalUtils.getAttr(member, ()-> member.getRealName(), null);
        log.debug("realName: {} ", realName);

        Integer memberId = OptionalUtils.getAttr(member, ()-> member.getMemberId(), null);
        log.debug("memberId: {} ", memberId);
    }

    /**
     * 测试从对象中获取某个值（对象是NULL）
     */
    @Test
    public void propertiesGetWithNullTest(){
        Member member = null;

        String attr = OptionalUtils.getAttr(member, ()-> member.getNickName(), null);
        log.debug("nickName: {} ", attr);

        String realName = OptionalUtils.getAttr(member, ()-> member.getRealName(), null);
        log.debug("realName: {} ", realName);

        Integer memberId = OptionalUtils.getAttr(member, ()-> member.getMemberId(), null);
        log.debug("memberId: {} ", memberId);
    }


}
