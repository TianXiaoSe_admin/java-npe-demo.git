package net.jhelp.demo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * <ul>
 *     Null数值转换工具类，减少业务代码中对 null != obj这样条件表达式的出现
 *     也可以减少类似三元表过式的出现。
 *     特色：使用Optional特性
 *     对ValueUtils的升级
 * </ul>
 *
 * @author : kame
 * @date: 2022/4/19 10:51 AM
 */
public class ValueUtilsV2 {

    /**
     * 转换null值的模板方法
     * @param value
     *  输入的值
     * @param defaultValue
     *  默认值
     * @param <R>
     *     输入的对象的类型
     * @return
     */
    public static <R> R wrapNull(R value, R defaultValue){
        return Optional.ofNullable(value).orElse(defaultValue);
    }

    /**
     * 转换BigDecimal（浮点数）数值可能为null的情况
     * @param value
     *   输入数值
     * @return
     *      默认返回2位数，四舍五入
     *      如果为null，则返回0
     */
    public static BigDecimal wrapNull(BigDecimal value){
        return wrapNull(value, 2, BigDecimal.ZERO);
    }

    /**
     * 转换BigDecimal（浮点数）数值可能为null的情况，如果为null，则返回0
     * @param value
     *      输入数值
     * @param scale
     *      指定小数位
     * @return
     *      默认四舍五入
     *      如果为null，则返回0
     */
    public static BigDecimal wrapNull(BigDecimal value, int scale){
        return wrapNull(value, scale, BigDecimal.ZERO);
    }

    /**
     * 转换BigDecimal（浮点数）数值可能为null的情况
     * @param value
     *      输入数值
     * @param scale
     *      指定小数位
     * @param defaultValue
     *      指定默认值
     * @return
     *      默认四舍五入
     *      如果为null，则返回 defaultValue
     */
    public static BigDecimal wrapNull(BigDecimal value, int scale, BigDecimal defaultValue){
        return Optional.ofNullable(value).map(e->e.setScale(scale, BigDecimal.ROUND_HALF_UP))
                .orElse(defaultValue);
    }

    /**
     * 转换输入的整数为null的情况
     * @param input
     *      输入数值
     * @return
     *      如果是null，则返回0
     */
    public static Integer nullInt(Integer input){
        return nullInt(input, 0);
    }

    /**
     * 转换输入的整数为null的情况
     * @param input
     *      输入数值
     * @param defaultValue
     *      指定默认值
     * @return
     *      如果是null，则返回 defaultValue
     */
    public static Integer nullInt(Integer input, Integer defaultValue){
        return Optional.ofNullable(input).orElse(defaultValue);
    }

    /**
     * 转换输入的长整数为null的情况
     * @param input
     *      输入数值
     * @return
     *      如果是null，则返回0
     */
    public static Long nullLong(Long input){
        return nullLong(input, 0L);
    }

    /**
     * 转换输入的长整数为null的情况
     * @param input
     *      输入数值
     * @param defaultValue
     *      指定默认值
     * @return
     *      如果是null，则返回 defaultValue
     */
    public static Long nullLong(Long input, Long defaultValue){
        return Optional.ofNullable(input).orElse(defaultValue);

    }

    /**
     * 转换输入的字符串为null的情况
     * @param input
     *      输入数值
     * @return
     *      如果是null，则返回空（""）
     */
    public static String nullString(String input){
        return nullString(input, "");
    }

    /**
     * 转换输入的字符串为null的情况
     * @param input
     *      输入数值
     * @param defaultValue
     *      指定默认值
     * @return
     *      如果是null，则返回  defaultValue
     */
    public static String nullString(String input, String defaultValue){
        return Optional.ofNullable(input).orElse(defaultValue);

    }

    /**
     * 格式化BigDecimal（浮点数）为字符串
     * @param input
     *      输入数值
     * @return
     *      默认返回2位小数，四舍五入
     *      如果是null，则返回（"0.00"）
     */
    public static String formatBigDecimal(BigDecimal input){
        return formatBigDecimal(input, 2);
    }

    /**
     * 格式化BigDecimal（浮点数）为字符串
     * @param input
     *      输入数值
     * @param scale
     *      指定小数位
     * @return
     *      默认 四舍五入
     *      如果是null，则返回（"0.00"）
     */
    public static String formatBigDecimal(BigDecimal input, int scale){
        return Optional.ofNullable(input)
                .map(e->String.valueOf(e.setScale(scale, BigDecimal.ROUND_HALF_UP)))
                .orElse("0.00");
    }

    /**
     * 格式化Date（日期）为字符串（yyyy-MM-dd）
     * @param input
     *      输入数值
     * @param pattern
     *      日期格式，如yyyy-MM-dd
     * @return
     *      如果是null，则返回空（""）
     */
    public static String formatDate(Date input, String pattern){
        return formatDate(input, pattern, "");
    }

    /**
     * 格式化Date（日期）为字符串
     * @param input
     *      输入数值
     * @param pattern
     *      日期格式，如yyyy-MM-dd
     * @param defaultDate
     *      指定的默认值
     * @return
     *      如果是null，则返回 defaultDate
     */
    public static String formatDate(Date input, String pattern, String defaultDate){
        return Optional.ofNullable(input).map(e->{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.format(input);
        }).orElse(defaultDate);
    }
}
